#!/usr/bin/env ruby
require 'colorize'

# if ARGV is zero (or no argument given) then default to 6. Else, keep ARGV.
length = ARGV[0].to_i.zero? ? 6 : ARGV[0].to_i

colors = String.colors
colors.delete(:black)

def scramble_pass(length)
  pass_dict = 'assets/diceware_dict.txt'

  # load new password as array and strip new lines
  pass_array = File.readlines(pass_dict)
  pass_array.map(&:strip!)

  # shuffle and strip to desired length.
  shuffle_pass = pass_array.shuffle[1..length]
  shuffle_pass
end

new_password = scramble_pass(length)
print 'Passphrase ~> '
new_password.each do |pass|
  rand_color = colors.shuffle[1]
  print pass.colorize(rand_color) + ' '
end
puts
