# Ruby Simple Diceware

Description
---
Simple Ruby diceware. Made with ♥ and little ruby knowledge ☺.
It has a lot of comments because I made this script with the objective to learn
more about the language, and also the necesity to change passwords often with
the recent breaches in every major site.

Requirements
---
`gem install colorize`

Installation
---
`git clone https://gitlab.com/franccesco/ruby-diceware.git`
`cd ruby-diceware`

Usage
---
`ruby rdice.rb`

`~> brig nsf m nina hale congo soap`

TODO
---
I want to implement this in a web framework, so we can easily make new passwords
without having to go to the console and execute the ruby script.

Also I need to refactor and improve my code.
